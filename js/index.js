
$(function(){
    $("[data-toggle='popover']").popover();
    $("[data-toggle='tooltip']").tooltip();
    $('.carousel').carousel({
        interval:3000
    });

    $('#contacto').on('show.bs.modal',function(e){
        console.log('El modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-default');
        $('#contactoBtn').prop('disabled',true);
    });
    $('#contacto').on('hide.bs.modal',function(e){
        console.log("se esta ocultando el modal")
        $('#contactoBtn').removeClass('btn-default');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled',false);
    });
    $('#contacto').on('shown.bs.modal',function(e){
        console.log("se muestra el modal shown")
    });
    $('#contacto').on('hidden.bs.modal',function(e){
        console.log("se ha ocultado el modal");
    });
});
